export const CONTEXT_REF = {
    NATIVE: 'native',
    WEBVIEW: 'webview',
};
class WebView {
    waitForWebViewContextLoaded () {
        driver.waitUntil(
            () => {
                const currentContexts = this.getCurrentContexts();

                return currentContexts.length > 1 &&
                    currentContexts.find(context => context.toLowerCase().includes(CONTEXT_REF.WEBVIEW));
            }, {
                timeout: driver.config.waitforTimeout,
                timeoutMsg: 'Webview context not loaded',
                interval: driver.config.waitforInterval,
            },
        );
    }

    switchToContext (context) {
        driver.switchContext(this.getCurrentContexts()[context === CONTEXT_REF.WEBVIEW ? 1 : 0]);
    }

    getCurrentContexts () {
        const contexts = driver.getContexts();
        return contexts;
    }
}

export default WebView;
