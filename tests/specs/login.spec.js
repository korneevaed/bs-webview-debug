import WebView from '../helpers/WebView';
import { CONTEXT_REF } from '../helpers/WebView';

const WebViewScreen = new WebView();

describe('The test', () => {
    it('shall click Get Started button', () => {
        WebViewScreen.waitForWebViewContextLoaded();
        WebViewScreen.switchToContext(CONTEXT_REF.WEBVIEW);

        const footer = $('.footer');
        footer.waitForDisplayed();
        const getStartedButton = footer.$('.button');
        getStartedButton.click();
        const emailInput = $('#email .native-input');
        emailInput.waitForDisplayed();
    });
});
