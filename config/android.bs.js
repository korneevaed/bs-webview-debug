const { config } = require('./shared.bs');

const androidCapabilities = [
    {        
        'bstack:options': {
            'realMobile': 'true',
        },
        // 'app' : process.env.BROWSERSTACK_APP_ANDROID,
        'platformName': 'Android',
        "os_version" : "9.0",
        "device" : "Samsung Galaxy S10",
        "app" : process.env.BROWSERSTACK_APP_ANDROID,
        "browserstack.appium_version" : "1.17.0",
    },
];

config.capabilities = [Object.assign(androidCapabilities[0], config.capabilities[0])];

exports.config = config;
