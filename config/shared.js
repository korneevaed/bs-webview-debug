exports.config = {
    user: process.env.BROWSERSTACK_USER,
    key: process.env.BROWSERSTACK_KEY,


    framework: 'jasmine',
    jasmineNodeOpts: {
        // Updated the timeout to 30 seconds due to possible longer appium calls
        // When using XPATH
        defaultTimeoutInterval: 90000,
        helpers: [require.resolve('@babel/register')],
    },
    sync: true,
    logLevel: 'debug',
    deprecationWarnings: true,
    bail: 0,
    waitforTimeout: 10000,
    waitforInterval: 100,
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
    reporters: [['allure', {
        outputDir: 'allure-results',
        disableWebdriverStepsReporting: false,
        disableWebdriverScreenshotsReporting: false,
    }]],

    // At the moment specs are common for iOS and Android.
    specs: [
    './tests/specs/**/*.spec.js',
    ],

    // Appium Configuration
    services: [
        [
            'appium',
            {
                logPath : './',
                args: {
                    // If specific version of chromedriver is needed, download it and provide absolute path inside this variable:
                    chromedriverExecutable: process.env.CHROMEDRIVER_PATH
                },
                command: 'appium',
            },
        ],
    ],
    port: 4723,
};
