const { config } = require('./shared');

config.capabilities = [
    {
        'project' : 'Mobile e2e autotests',
        'build' : `webview-debug`,
        'name' : 'Name'
    },
];

config.services = ['browserstack'];

// This port was defined in the `shared.js`
delete config.port;

exports.config = config;
